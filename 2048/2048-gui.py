import pygame
from pygame import locals
import random
import copy

matrix = [[0 for i in range(4)] for i in range(4)]
block_wh = 101
block_space = 14


def game_init(lst, num=1):
    amount = 0
    while True:
        x = random.randint(0, 3)
        y = random.randint(0, 3)
        n = 2 if random.random() < 0.8 else 4
        if lst[x][y] == 0:
            lst[x][y] = n
            amount += 1
            if amount >= num:
                return lst

def draw_surface(screen, lst):
    for x in range(4):
        for y in range(4):
            draw_block(screen, x, y, lst[x][y])

def draw_block(screen, x, y, vlaue):
    if vlaue != 0:
        h = x * block_wh + (x + 1) * block_space + 145
        w = y * block_wh + (y + 1) * block_space + 15
        num_image = pygame.image.load(f'images/{vlaue}.jpg')
        screen.blit(num_image, (w, h))

def action(keyvalue, lst):
    if keyvalue == pygame.K_LEFT:
        return leftaction(lst)
    elif keyvalue == pygame.K_RIGHT:
        return rightaction(lst)
    elif keyvalue == pygame.K_UP:
        return upaction(lst)
    elif keyvalue == pygame.K_DOWN:
        return downaction(lst)

def leftaction(lst):
    mid = copy.deepcopy(lst)
    for row in range(4):
        lst[row] = removezero(lst[row])
    if lst == mid:
        return lst
    game_init(matrix)

def rightaction(lst):
    transpose(lst)
    leftaction(lst)
    transpose(lst)

def upaction(lst):
    reverse(lst)
    leftaction(lst)
    rreverse(lst)

def downaction(lst):
    rreverse(lst)
    leftaction(lst)
    reverse(lst)

def reverse(lst):
    mid = copy.deepcopy(lst)
    for i in range(4):
        for j in range(4):
            lst[i][j] = mid[j][3 - i]
    return lst

def rreverse(lst):
    mid = copy.deepcopy(lst)
    for i in range(4):
        for j in range(4):
            lst[i][j] = mid[3 - j][i]
    return lst

def transpose(lst):
    mid = copy.deepcopy(lst)
    for i in range(4):
        for j in range(4):
            lst[i][j] = mid[i][3 - j]
    return lst

def removezero(row):
    while True:
        mid = row[:]
        try:
            row.remove(0)
            if len(row) < 4:
                row.append(0)
        except:
            pass
        if row == mid:
            break
    return combinelist(row)

def combinelist(row):
    start_num = 0
    if row[0] == 0:
        return row
    end_num = 4 - row.count(0) - 1
    while start_num < end_num:
        if row[start_num] == row[start_num + 1]:
            row[start_num] *= 2
            row[start_num + 1:] = row[start_num + 2:]
            row.append(0)
        start_num += 1
    return row

def main():
    pygame.init()
    screen = pygame.display.set_mode((504, 634))
    pygame.display.set_caption("2048 game")
    sur_image = pygame.image.load('images/surface.jpg').convert()
    screen.blit(sur_image, (0, 0))
    game_init(matrix, num=2)
    draw_surface(screen, matrix)
    pygame.display.update()

    while True:
        screen.blit(sur_image, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN:
                action(event.key, matrix)
        draw_surface(screen, matrix)
        pygame.display.update()


if __name__ == '__main__':
    main()